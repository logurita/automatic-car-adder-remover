# Requirements

Requires Node.js and Modloader

# Instalation

That project was based on node, so you will need to install node.js first. After that, you could be able to run needed commands on a command line (cmd).

First step run command `npm install` at root of that folder.

# Use:

There are diferent commands to execute actions:

## Step 1:

Adding a car needs just required .dff and .txd files. That files needs to be named exactly as original model it replace (it's not a replace). That files needs to be placed in cartoadd folder, there is an example there.

## Step 2:

Configuration of posible actions are required. Configuration files are that 4 .json files in root folder. There are 4 posible actions:

- add a car
- remove a car
- list id of added cars (it will be saved as ids.txt in root of that folder) 
- replace id of an added car to a new one

### Notes

- numericID property needs to use a free id number in game (research about id generator, and car spawner).
- carID property needs to be a word with maximum length 8 characters.
- carName property is complete name of added car and name of folder containing your files in modloader folder.
- carMods section has a property named `useDefault`. Value true will make carMod value to your car as: `to_b_l, nto_b_s, nto_b_tw`. Value as false, will use replaced vehicle original value to your new car value.
- cargrp section has a property named `placeAtSameGroupAsOriginalModel`. Value true will place your new car in same groups as replaced vehicle do. Value false will force to use property `groups` configuration.
`groups` configuration has every group conained in original `cargrp.dat` file as a property that accepts true or false value. True value makes car added to that specific group.

## Step 3:

To execute operations, just run following commands on your command line (cmd) at root of that folder.

- `npm run add` --> it will add a new car
- `npm run remove` --> it will remove a car
- `npm run list-id` --> it will list ids of added cars (it will be saved as ids.txt in root of that folder) 
- `npm run modify-id` --> it will replace id of an added car to a new one

