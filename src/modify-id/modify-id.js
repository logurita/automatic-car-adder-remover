#!/usr/bin/env node

const fs = require('fs');

const path = require('path');

const common = require('./common.js');

listIds();

function listIds() {
    const ids = [];
    fs.readdirSync(common.config.modLoaderPath).forEach((folder) => {
        const carFolder = path.join(common.config.modLoaderPath, folder);
        if (fs.lstatSync(carFolder).isDirectory()) {
            if (fs.existsSync(path.join(carFolder, 'carData.txt'))) {
                const file = fs.readFileSync(path.join(carFolder, 'carData.txt')).toString();
                if (file.indexOf(common.config.oldCarID.toString()) !== -1) {
                    fs.writeFile(path.join(carFolder, 'carData.txt'), file.replace(common.config.oldCarID.toString(), common.config.newCarID.toString()), () => null);
                }
            }
        }
    })
}