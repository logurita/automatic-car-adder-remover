#!/usr/bin/env node

const fs = require('fs');

const path = require('path');

const common = require('./common.js');

listIds();

function listIds() {
    const ids = [];
    fs.readdirSync(common.config.modLoaderPath).forEach((folder) => {
        const carFolder = path.join(common.config.modLoaderPath, folder);
        if (fs.lstatSync(carFolder).isDirectory()) {
            const carID = getCarID(carFolder);
            if (carID && fs.existsSync(path.join(carFolder, 'carData.txt'))) {
                const file = fs.readFileSync(path.join(carFolder, 'carData.txt')).toString().split('\n');
                for (let i = 0; i < file.length; i++) {
                    if (file[i].indexOf(carID) !== -1) {
                        ids.push({ [carID]: file[i].split(',')[0] });
                        break;
                    }
                }
            }
        }
    })
    fs.writeFile('./ids.txt', JSON.stringify(ids, null, 2), () => null);
}

function getCarID(carFolder) {
    if (fs.existsSync(path.join(carFolder, 'carname.fxt'))) {
        const file = fs.readFileSync(path.join(carFolder, 'carname.fxt')).toString();
        return file.split(' ')[0];
    }
}