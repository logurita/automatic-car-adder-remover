#!/usr/bin/env node

const fs = require('fs');

const path = require('path');

const rimraf = require('rimraf');

const common = require('./common.js');

rimraf(common.carFolder, () => null);

removeFromCargrp();

removeFromCarmods();

removeFromCarAudioSettings();

function removeFromCarAudioSettings() {
    const caraudioFile = fs.readFileSync(common.caraudioFile).toString();
    if (caraudioFile.indexOf(common.config.carID) === -1) {
        return;
    }
    const split = caraudioFile.split(common.config.carID)[1];
    const lineToRemove = common.config.carID + split.substring(0, split.indexOf('\n'));
    const trim = getLineDropPosition(caraudioFile, lineToRemove);
    const carAudioFileModified = caraudioFile.replace(trim, '');
    fs.writeFile(common.caraudioFile, carAudioFileModified, () => null);
}

function removeFromCargrp() {
    const cargrp = fs.readFileSync(common.cargrpFile).toString();
    if (cargrp.indexOf(common.config.carID) === -1) {
        return;
    }
    const removeRegex = new RegExp(common.config.carID + ', ', 'g');
    const cargrpModified = cargrp.replace(removeRegex, '');
    fs.writeFile(common.cargrpFile, cargrpModified, () => null);
}

function removeFromCarmods() {
    const carMods = fs.readFileSync(common.carmodsFile).toString();
    if (carMods.indexOf(common.config.carID) === -1) {
        return;
    }
    const split = carMods.substring(carMods.indexOf(common.config.carID));
    const lineToRemove = split.substring(split, split.indexOf('\n'));
    let trim = getLineDropPosition(carMods, lineToRemove);
    fs.writeFile(common.carmodsFile, carMods.replace(trim, ''), () => null);
}

function getLineDropPosition(file, match) {
    if (file.indexOf('\n' + match) !== -1) {
        return '\n' + match;
    } else if (file.indexOf(match + '\n') !== -1) {
        return match + '\n';
    }
    return match;
}