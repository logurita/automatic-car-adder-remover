#!/usr/bin/env node

const config = require("../../config-remove.json");

module.exports = {
    config: config,

    carFolder: config.modLoaderPath + config.carName,

    handlingFile: config.gameFolder + 'data/handling.cfg',

    vehicleIdeFile: config.gameFolder + 'data/vehicles.ide',

    carcolsFile: config.gameFolder + 'data/carcols.dat',

    cargrpFile: config.gameFolder + 'data/cargrp.dat',

    carmodsFile: config.gameFolder + 'data/carmods.dat',

    caraudioFile: config.gameFolder + 'data/gtasa_vehicleAudioSettings.cfg'
}