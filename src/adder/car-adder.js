#!/usr/bin/env node

const fs = require('fs');

const path = require('path');

const rimraf = require('rimraf');
// 
const common = require('./common.js');

rimrafSync(common.carFolder);

copyCarFiles();

createModFile();

includeToCargrp();

includeToCarmods();

includeToCarAudioSettings();

// rimrafSync(carFolder);

function copyCarFiles() {
  fs.mkdirSync(common.carFolder);
  fs.writeFileSync(path.join(common.carFolder, 'carname.fxt'), common.config.carID + " " + common.config.carName);
  const fileListDff = [];
  fromDir(path.join(process.cwd(), 'cartoadd'), 'dff', fileListDff);
  setBaseModel(fileListDff[0]);
  const fileListTxd = [];
  fromDir(path.join(process.cwd(), 'cartoadd'), 'txd', fileListTxd);
  const fileList = fileListDff.concat(fileListTxd);
  fileList.forEach((file) => {
    fs.copyFile(file, path.join(common.carFolder, renameFiles(file)), () => null);
  });
}

function setBaseModel(modelFilePath) {
  const modelFile = modelFilePath.split('\\')[modelFilePath.split('\\').length - 1];
  common.config.baseModel = modelFile.substring(0, modelFile.indexOf('.dff')).toLowerCase();
}

function renameFiles(file) {
  if (file.indexOf(common.config.baseModel)) {
    const carName = file.substring(file.indexOf(common.config.baseModel));
    return carName.replace(common.config.baseModel, common.config.carID);
  }
}

function createModFile() {
  const handlingLine = getHandlingLine();
  const vehicleIdeLine = getVehicleIdeLine();
  const carcolsLine = getCarcolsLine();
  fs.writeFile(path.join(common.carFolder, 'carData.txt'), handlingLine + '\n' + vehicleIdeLine + '\n' + carcolsLine, () => null);
}

function includeToCarAudioSettings() {
  const caraudioFile = fs.readFileSync(common.caraudioFile).toString();
  if(caraudioFile.indexOf(common.config.carID) !== -1) {
    return;
  }
  const split = caraudioFile.split(common.config.baseModel)[1];
  const line = split.substring(0, split.indexOf('\n'));
  const lineToAdd = common.config.carID + line;
  const carAudioFileModified = caraudioFile.replace(';the end', lineToAdd + '\n;the end');
  fs.writeFile(common.caraudioFile, carAudioFileModified, () => null);
}

function includeToCarmods() {
  const carMods = fs.readFileSync(common.carmodsFile).toString();
  if(carMods.indexOf(common.config.carID) !== -1) {
    return;
  }
  let carmodsInfo = common.config.carID + ', to_b_l, nto_b_s, nto_b_tw';
  if (!common.config.carMods.useDefault) {
    const startPos = carMods.substring(carMods.indexOf(common.config.baseModel) + common.config.baseModel.length);
    carmodsInfo = common.config.carID + startPos.substring(startPos, startPos.indexOf('\n'));
  }
  const carArea = carMods.split('mods')[3].split('\nend')[0];
  addedCarArea = carArea + carmodsInfo;
  fs.writeFile(common.carmodsFile, carMods.replace(carArea, addedCarArea), () => null);
}

function includeToCargrp() {
  const cargrp = fs.readFileSync(common.cargrpFile).toString();
  let carGrpModified;
  if (cargrp.indexOf(common.config.carID) !== -1) {
    return;
  }
  if (common.config.cargrp.placeAtSameGroupAsOriginalModel) {
    carGrpModified = addWithOriginalModel(cargrp);
  } else {
    carGrpModified = addWithSpecificconfig(cargrp);
  }
  fs.writeFile(common.cargrpFile, carGrpModified, () => null);
}

function addWithSpecificconfig(cargrp) {
  const groupObject = common.config.cargrp.groups;
  const groupsList = Object.keys(groupObject);

  groupsList.filter((i) => groupObject[i]).forEach((group) => {
    const line = cargrp.split(group)[0].split('\n')[cargrp.split(group)[0].split('\n').length - 1];
    const lastCar = line.split(',')[line.split(',').length - 1]
    const addedCar = ' ' + common.config.carID + ',' + lastCar;
    const lineReplaced = line.replace(lastCar, addedCar);
    cargrp = cargrp.replace(line, lineReplaced);
  });
  return cargrp;
}

function addWithOriginalModel(cargrp) {
  const baseRegex = new RegExp(common.config.baseModel, 'g');
  return cargrp.replace(baseRegex, common.config.baseModel + ', ' + common.config.carID);
}

function getCarcolsLine() {
  const fileList = fs.readFileSync(common.carcolsFile).toString().split('\n');
  const line = fileList.filter((line) => line.indexOf(common.config.baseModel) !== -1)[0];
  return line.replace(common.config.baseModel, common.config.carID);
}

function getVehicleIdeLine() {
  const fileList = fs.readFileSync(common.vehicleIdeFile).toString().split('\n');
  const line = fileList.filter((line) => line.indexOf(common.config.baseModel) !== -1)[0];
  const segments = line.split(',');
  segments[0] = common.config.numericID;
  segments[1] = common.config.carID;
  segments[2] = common.config.carID;
  segments[4] = common.config.carID.toUpperCase();
  segments[5] = common.config.carID.toUpperCase();
  return segments.join(',      ');
}

function getHandlingLine() {
  const fileList = fs.readFileSync(common.handlingFile).toString().split('\n');
  const line = fileList.filter((line) => line.indexOf(common.config.baseModel.toUpperCase()) !== -1)[0];
  return line.replace(common.config.baseModel.toUpperCase(), common.config.carID.toUpperCase());
}

function fromDir(startPath, filter, list) {

  //console.log('Starting from dir '+startPath+'/');

  if (!fs.existsSync(startPath)) {
    console.log("no dir ", startPath);
    return;
  }

  var files = fs.readdirSync(startPath);
  for (var i = 0; i < files.length; i++) {
    var filename = path.join(startPath, files[i]);
    var stat = fs.lstatSync(filename);
    if (stat.isDirectory()) {
      fromDir(filename, filter); //recurse
    }
    else if (filename.indexOf(filter) >= 0) {
      list.push(filename);
    };
  };
};

function rimrafSync(item) {
  try {
    rimraf.sync(item);
  } catch (e) {
    return;
  }
}